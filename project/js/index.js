new Glide('.glide', {
    type: 'carousel',
}).mount();

let tabDescription = document.querySelector(".tabs_list");

let btnLoad = document.querySelector(".btn_load_image");

let gifLoad = document.querySelector(".gif-load");

let allimg = document.querySelectorAll(".box_gallery_img");

let tabCategory = document.querySelector(".photo_gallery_list");

function switchTabs() {

    tabDescription.addEventListener("click", (event) => {

        let targetEvent = event.target.dataset.currentTarget;

        document.querySelectorAll(".tabs_item, .tabs_box").forEach(element => {

            element.classList.remove("active")
        });
        event.target.classList.add("active");

        document.querySelector("." + targetEvent).classList.add("active");
    })
}
switchTabs();

function uploadingPhotos() {

    btnLoad.addEventListener("click", () => {

        gifLoad.classList.add("active-load");

        btnLoad.style.display = "none";

        setTimeout(() => {

            gifLoad.classList.remove("active-load");

            btnLoad.style.display = "block";

            allimg.forEach(element => {

                if (element.classList.contains("off")) {

                    element.classList.remove("off");

                    btnLoad.style.display = "none";
                }
            });
        }, 7000);
    });
}
uploadingPhotos();

function photosCategory() {

    tabCategory.addEventListener("click", (event) => {

        let eventTarget = event.target.dataset.currentTarget;

        allimg.forEach(element => {

            if (eventTarget == element.dataset.currentTarget) {

                element.style.display = "block"

                btnLoad.style.display = "none";

            } else if (eventTarget !== element.dataset.currentTarget) {

                element.style.display = "none"
            }
        });

        if (eventTarget == 'all') {

            for (let index = 0; index < 12; index++) {

                const element = allimg[index].style.display = "block";
            }

            btnLoad.style.display = "block";

            btnLoad.addEventListener("click", () => {

                gifLoad.classList.add("active-load");

                btnLoad.style.display = "none";

                setTimeout(() => {

                    gifLoad.classList.remove("active-load");

                    allimg.forEach(element => {

                        element.style.display = "block";

                    });
                }, 7000);

                btnLoad.style.display = "none";
            })
        }
    })

}
photosCategory();
