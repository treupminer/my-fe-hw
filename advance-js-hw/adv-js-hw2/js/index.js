
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let root = document.querySelector('#root');
let ul = document.createElement('ul');
root.append(ul);

function exclusion(message) {
    this.message = message;
    this.name = "Исключение, определенное пользователем";
}

function list(books) {
    for (let i = 0; i < books.length; i++) {
        try {
            elements(books, i);
        } catch (error) {
            console.log(error);
        }
    }
}

function elements(books, index) {
    const {author, name, price} = books[index];
    const arr = [author, name, price];


    if (!arr.includes(undefined)) {
        const bc = document.createElement('li');
        ul.append(bc);
        bc.innerText = `автор: ${author}, наименование: "${name}", цена: ${price}`;
    } else {
        throw new exclusion(`undefined field at books[${index}] at obj key[${arr.indexOf(undefined)}]`);
    }
}

list(books);