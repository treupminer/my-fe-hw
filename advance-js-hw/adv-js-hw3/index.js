class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        if (!value) { throw new Error('Invalid name');}
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (!value) { throw new Error('Invalid age');}
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (!value) { throw new Error('Invalid salary'); }
        this._salary = value;
    }
}

let prog = new Employee('Oleg', 25, 12000);
console.log(prog);


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age);
        this._name = name;
        this._age = age;
        this._salary = salary;
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(value) {
        if (!value) { throw new Error('Invalid salary'); }
        this._salary = value;
    }
    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
}

let programmer1 = new Programmer('Grisha', 29, 15000, 'JavaScript');
console.log(programmer1);

let programmer2 = new Programmer('Taras', 27, 11500, 'Php');
console.log(programmer2);

let programmer3 = new Programmer('Vugar', 25, 20000, 'C++');
console.log(programmer3);

