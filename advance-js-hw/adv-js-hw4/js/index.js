const urlRequest = 'http://swapi.dev/api/films';
const re = document.querySelector('.blank');

async function requestListAll(url) {
    let response = await fetch(url);
    if (response.ok) {
        let json = await response.json();
        let list = await json.results;
        console.log(list)
        list.forEach(ul => {
            createListAll(ul);
        })
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

requestListAll(urlRequest);

function createListAll(ul) {
    let blank = document.createElement('blank');
    let {title, episode_id, opening_crawl} = ul;
    blank.innerHTML = `Title: ${title},<br>
    Episode: ${episode_id},<br>
    Description: <i>${opening_crawl}</i>`
    re.append(blank);
    let re1 = document.createElement('re');
    re1.innerHTML = '<b>Characters:</b>'
    blank.append(re1);
    ul.characters.forEach(item => {
        requestCharacters(item, re1);
    })
}

async function requestCharacters(url, re1) {
    let response = await fetch(url);
    if (response.ok) {
        let json = await response.json();
        let blank2 = document.createElement('blank');
        blank2.innerHTML = json.name;
        re1.append(blank2)
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

