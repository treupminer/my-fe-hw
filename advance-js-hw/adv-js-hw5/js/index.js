let dataJson = {};
const requestAPI = 'http://api.ipify.org/?format=json';
let table = document.querySelector('.table');
let button = document.querySelector('.button');
let tableData = document.querySelectorAll('.table__td');


async function requestIP(url) {
    let response = await fetch(url);

    if (response.ok) {
        let json = await response.json();
        let requestUrlData = `http://ip-api.com/json/${json.ip}?fields=status,message,continent,country,region,city,district,query`
        requestData(requestUrlData);
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

async function requestData(url) {
    let response = await fetch(url);

    if (response.ok) {
        dataJson = await response.json();

        tableData.forEach(el => {
            if (el.dataset.name !== undefined) {
                for (const key in dataJson) {
                    if (el.dataset.name === key) {
                        el.innerHTML = dataJson[key]
                        table.classList.add('active');
                    }
                }
            }
        })
    } else {
        alert("Ошибка HTTP: " + response.status)
    }
}

button.addEventListener('click', () => {
    requestIP(requestAPI);
})


