"use strict"

function changeColor() {

	let wrapper = document.querySelector(".wrapper-page-bgr");

	let switchColors = document.querySelector(".check-one");

	if (localStorage.getItem("backgroundColor") !== null) {

		let saveBackground = localStorage.getItem("backgroundColor");

		wrapper.style.background = saveBackground;
	}

	switchColors.addEventListener("click", () => {

		if (wrapper.classList.contains("wrapper-page-bgr")) {

			wrapper.classList.remove("wrapper-page-bgr");

			wrapper.style.background = "orange";

			localStorage.setItem("backgroundColor", "orange")
			
		} else {

			wrapper.classList.add("wrapper-page-bgr");

			wrapper.style.background = "white";

			localStorage.setItem("backgroundColor", "white")
		}

	})

}
changeColor();
