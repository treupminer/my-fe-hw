function createNewUser() {

    let firstName = prompt("Enter you name", "");

    let lastName = prompt("Enter you lastname", "");

    let birthday = prompt("Enter your date of birth", "dd.mm.yyyy");

    let newUser = {

        firstName,

        lastName,

        birthday,

        getLogin() {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },

        getAge() {

            let getUserDay = this.birthday.slice(0, 2);

            let getUserMonth = this.birthday.slice(3, 5) - 1;

            let getUserYear = this.birthday.slice(6);

            let dateBirth = new Date(getUserYear, getUserMonth, getUserDay);

            let now = new Date();

            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            let dateBirthNow = new Date(today.getFullYear(), dateBirth.getMonth(), dateBirth.getDate());

            let age = today.getFullYear() - dateBirth.getFullYear();

            if (today < dateBirthNow) {
                age -= 1;
            }

            return age;
        },

        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6)}`;
        },
    };
    return newUser;
}

let showObject = createNewUser();

console.log(showObject);

console.log(showObject.getLogin());

console.log(showObject.getAge());

console.log(showObject.getPassword());
