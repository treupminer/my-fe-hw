let container = document.createElement("div");

container.classList.add("container");

document.body.insertAdjacentElement("afterbegin", container);

let form = document.createElement("form");

form.classList.add("form");

container.insertAdjacentElement("afterbegin", form);

let label = document.createElement("label");

label.classList.add("label");

form.insertAdjacentElement("afterbegin", label);

let input = document.createElement("input");

label.insertAdjacentElement("afterbegin", input);

input.insertAdjacentHTML("beforebegin", "Price:");

input.setAttribute("type", "text");

input.setAttribute("value", "");

input.classList.add("input");

let alert = document.createElement("div");

alert.classList.add("alert");

form.insertAdjacentElement("afterbegin", alert);

let span = document.createElement("span");

span.classList.add("span");

span.insertAdjacentHTML("afterbegin", "Current price: &dollar;");
alert.insertAdjacentElement("afterbegin", span);

let spanReset = document.createElement("span");

spanReset.classList.add("span-reset");

spanReset.insertAdjacentHTML("afterbegin", "&#9587;");

alert.insertAdjacentElement("beforeend", spanReset);

let spanError = document.createElement("span");

spanError.classList.add("span-error");

spanError.insertAdjacentHTML("afterbegin", "Please enter correct price.");

form.insertAdjacentElement("beforeend", spanError);

let inputValue;

input.addEventListener("focusin", function () {

    form.insertAdjacentElement("afterbegin", alert);

    form.insertAdjacentElement("beforeend", spanError);

    input.classList.toggle("green-border");

    alert.style.visibility = "hidden";

    spanError.style.visibility = "hidden";

    input.classList.remove("value");

    input.classList.remove("red-border");

    input.value = " ";
});

input.addEventListener("focusout", function () {

    inputValue = input.value;

    if (inputValue > 0 || inputValue == Number) {

        input.classList.remove("green-border");

        alert.style.visibility = "visible";

        span.textContent += ` ${inputValue}`;

        input.classList.toggle("value");

    } else if (inputValue < 0 || inputValue != Number) {

        input.classList.toggle("red-border");

        spanError.style.visibility = "visible";
    }
});

spanReset.addEventListener("click", function () {

    input.classList.remove("value");

    alert.parentNode.removeChild(alert);

    spanError.parentNode.removeChild(spanError);

    input.value = " ";
});