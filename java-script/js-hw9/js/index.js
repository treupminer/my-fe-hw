+
    function () {

    document.querySelector(".item_tab").classList.add("active");

    document.querySelector(".content_block").classList.add("active");

    document.querySelectorAll(".item_tab").forEach(element => {

        element.addEventListener("click", function eventHandler(event) {

            let target = event.target.dataset.currentTarget;

            document.querySelectorAll(".item_tab, .content_block").forEach(element => {

                element.classList.remove("active")
            });

            event.target.classList.add("active");

            document.querySelector("." + target).classList.add("active");
        });
    });

}();
