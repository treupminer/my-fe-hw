let number = +prompt ('Enter number', '');

if (number > 1) {
    for (let n = 1; n <= number; n++) {

        if (n % 5 === 0) {
            console.log(n);

        } else if (number < 5 && number !== 0) {
            console.log("Sorry, no numbers");
        }
    }
} else if (number < 0) {
    for (let i = 0; i >= number; i--) {

        if (i % 5 === 0) {
            console.log(i);

        } else if (number > -5 && number !== 0) {
            console.log("Sorry, no numbers");
        }
    }
}